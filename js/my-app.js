$(document).ready(function() {
    $('div').click(function () {
        var url = $(this).attr('rel');
        $('#iframe').attr('src', url);
        $('#iframe').reload();
    });

    $("img").lazyload({
        effect: 'fadeIn',
        threshold: 200
    });
});



$('#example1').arctext({radius: 1500});
$('#example2').arctext({radius: 1500});

$('#semanas').countdown('2018/06/22', function(event) {
    $(this).html(event.strftime('%w'));
});
$('#dias').countdown('2018/06/22', function(event) {
    $(this).html(event.strftime('%d'));
});
$('#horas').countdown('2018/06/22', function(event) {
    $(this).html(event.strftime('%H'));
});
$('#minutos').countdown('2018/06/22', function(event) {
    $(this).html(event.strftime('%M'));
});
$('#segundos').countdown('2018/06/22', function(event) {
    $(this).html(event.strftime('%S'));
});